import React, {useEffect} from 'react';
import {Text, Image, View, StyleSheet} from 'react-native';
import Logo from '../../assets/Bareng_Saya_White.png';

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Welcome');
        }, 2500);
    });
    return (
        <View style={styles.wrapper}>
            <Image source={Logo} style={styles.logo}/>
        </View>
    );
};

export default Splash;

const styles = StyleSheet.create({
    wrapper: {
        flex : 1,
        backgroundColor : '#0b809c',
        alignItems : 'center',
        justifyContent : 'center',
    },
    logo : {
        margin : 10,
        resizeMode : 'contain',
        height : '30%',
    },
    welcomeText : {
        fontSize : 24,
        fontWeight : 'bold',
        color : 'green',
        paddingBottom : 20,
    },
});