import React, {useEffect} from 'react';
import {TextInput, View, TouchableOpacity, Text, StyleSheet, Image} from 'react-native';
import Logo from '../../assets/Bareng_Saya.png';
import Google from '../../assets/Google.png';

const WelcomeAuth = () => {
    return (
        <View style={styles.wrapper}>
            <Image source={Logo} style={styles.logo}/>
            <TextInput 
                style={styles.form}
                placeholder = "Username or Email"
            />
            <TextInput 
                style={styles.form}
                placeholder = "Password"
            />
            <TouchableOpacity style={styles.login} onPress={null}>
                <Text style={{color : 'white', textAlign : 'center', position : 'absolute'}}>LOGIN</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={null}>
                <Text style={{fontSize: 15, color : '#0b809c',}}>Forgot email / password?</Text>
            </TouchableOpacity>
            <Text style={{fontSize: 15, color : '#0b809c', margin : 20, marginBottom : 0}}>OR</Text>
            <TouchableOpacity style={styles.buttonGoogle} onPress={null}>
                <Text style={{fontSize : 15, fontWeight : 'bold', color : 'white', textAlign : 'left', position : 'absolute'}}>Continue with Google</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonGithub} onPress={null}>
                <Text style={{fontSize : 15, fontWeight : 'bold', color : 'white', textAlign : 'left', position : 'absolute'}}>Continue with GitHub</Text>
            </TouchableOpacity>
            <Text style={styles.text}>Copyright {'\u00A9'} 2021 BarengSaya</Text>
        </View>
    );
};

export default WelcomeAuth;

const styles = StyleSheet.create({
    wrapper: {
        flex : 1,
        flexDirection : 'column',
        backgroundColor : 'white',
        alignItems : 'center',
        justifyContent : 'center',
        position : 'relative',
    },
    form : {
        height : 40,
        width : 250,
        backgroundColor : '#ededed',
        textAlign : 'left',
        borderRadius : 100,
        margin : 3,
        paddingLeft : 20,
    },
    login : {
        alignItems : 'center',
        backgroundColor : '#0b809c',
        borderRadius : 100,
        height : 40,
        width : 200,
        alignItems : 'center',
        margin : 25,
        justifyContent : 'center',
        paddingTop : 0,
    },
    logo : {
        resizeMode : 'contain',
        height : '30%',
        position : 'absolute',
        top : 0,
    },
    googleIcon : {
        resizeMode : 'contain',
        height : 20,
        position : 'absolute',
    },
    text : {
        color : '#0b809c',
        bottom : 10,
        position : 'absolute',
    },
    buttonGoogle : {
        display : 'flex',
        flex :0,
        flexDirection : 'row',
        alignItems : 'center',
        justifyContent : 'center',
        borderRadius : 100,
        // borderWidth : 2,
        width : 200,
        height : 40,
        flexWrap : 'wrap',
        marginTop : 30,
        backgroundColor : '#4285F4',
    },
    buttonGithub : {
        display : 'flex',
        flex :0,
        flexDirection : 'row',
        alignItems : 'center',
        justifyContent : 'center',
        borderRadius : 100,
        // borderWidth : 2,
        borderColor : 'black',
        width : 200,
        height : 40,
        flexWrap : 'wrap',
        marginTop : 5,
        backgroundColor : 'black',
    },
});