import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Splash from '../pages/splash';
import WelcomeAuth from '../pages/welcome';

const Stack = createStackNavigator();

const Route = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Splash"
                component={Splash}
                options={{headerShown: false}}
            />
            <Stack.Screen
                name="Welcome"
                component={WelcomeAuth}
                options={{headerShown: false}}
            />
        </Stack.Navigator>
    );
};

export default Route;